export class Timer {
  /**
   * @type {'STOPPED' | 'RUNNING' | 'PAUSED'}
   */
  _currentStatus = 'STOPPED';
  _isSitting = true;
  _elapsedSeconds = 0;

  _tickInterval = setInterval(() => this.tick(), 1000);

  _sittingSeconds = 0;
  _standingSeconds = 0;

  _onTick = () => {};
  _onStateChange = () => {};

  /**
   * @param {() => void} onTick
   * @param {() => void} onStateChange
   */
  constructor(onTick, onStateChange) {
    this._onTick = onTick;
    this._onStateChange = onStateChange;

    this.restoreState();
  }

  saveState() {
    localStorage.setItem(
      'state',
      JSON.stringify({
        currentStatus: this._currentStatus,
        isSitting: this._isSitting,
        elapsedSeconds: this._elapsedSeconds,
      }),
    );
  }

  restoreState() {
    const value = localStorage.getItem('state');
    if (!value) return;

    const state = JSON.parse(value);

    this._currentStatus = state.currentStatus;
    this._isSitting = state.isSitting;
    this._elapsedSeconds = state.elapsedSeconds;
  }

  tick() {
    if (this._currentStatus !== 'RUNNING') return;

    this._elapsedSeconds += 1;

    if (this._isSitting && this._elapsedSeconds > this._sittingSeconds) {
      this.next();
      this._onStateChange();
    } else if (
      !this._isSitting &&
      this._elapsedSeconds > this._standingSeconds
    ) {
      this.next();
      this._onStateChange();
    }

    this._onTick();

    this.saveState();
  }

  start() {
    this._currentStatus = 'RUNNING';
  }

  stop() {
    this._currentStatus = 'STOPPED';
    this._elapsedSeconds = 0;
  }

  pause() {
    this._currentStatus = 'PAUSED';
  }

  next() {
    this._isSitting = !this._isSitting;
    this._elapsedSeconds = 0;
  }

  /**
   * @param {number} seconds
   */
  setStandingSeconds(seconds) {
    this._standingSeconds = seconds;
  }

  /**
   * @param {number} seconds
   */
  setSittingSeconds(seconds) {
    this._sittingSeconds = seconds;
  }

  getCurrentStatus() {
    return this._currentStatus;
  }

  getElapsedSeconds() {
    return this._elapsedSeconds;
  }

  getIsSitting() {
    return this._isSitting;
  }
}

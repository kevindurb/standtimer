import { Timer } from './Timer.mjs';
import { View } from './View.mjs';

export class StandTimer {
  /** @type {Timer} */
  timer;
  /** @type {View} */
  view;

  constructor() {
    this.timer = new Timer(
      () => this.tick(),
      () => this.onStateChange(),
    );
    this.view = new View(
      () => this.start(),
      () => this.stop(),
      () => this.pause(),
      () => this.next(),
      () => this.updateSittingValue(),
      () => this.updateStandingValue(),
    );
    this.renderButtonsDisabled();
    this.updateStandingValue();
    this.updateSittingValue();
  }

  onStateChange() {
    new Notification(`Time to ${this.timer.getIsSitting() ? 'sit' : 'stand'}!`);
  }

  tick() {
    this.renderHeader();
  }

  updateStandingValue() {
    const minutes = this.view.getStandingDurationValue();
    this.timer.setStandingSeconds((minutes ?? 0) * 60);
    this.view.updateStandingValue(minutes);
  }

  updateSittingValue() {
    const minutes = this.view.getSittingDurationValue();
    this.timer.setSittingSeconds((minutes ?? 0) * 60);
    this.view.updateSittingValue(minutes);
  }

  renderHeader() {
    this.view.renderHeader(
      this.timer.getIsSitting(),
      this.timer.getElapsedSeconds(),
    );
  }

  renderButtonsDisabled() {
    this.view.renderButtonsDisabled(this.timer.getCurrentStatus());
  }

  start() {
    if (Notification.permission === 'default') {
      Notification.requestPermission();
    }
    this.timer.start();
    this.renderButtonsDisabled();
  }

  stop() {
    this.timer.stop();
    this.renderButtonsDisabled();
  }

  pause() {
    this.timer.pause();
    this.renderButtonsDisabled();
  }

  next() {
    this.timer.next();
  }
}

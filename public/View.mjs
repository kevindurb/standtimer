export class View {
  /**
   * @type {HTMLHeadingElement}
   */
  $heading = document.getElementById('heading');
  /**
   * @type {HTMLHeadingElement}
   */
  $standingLabel = document.getElementById('standing-label');
  /**
   * @type {HTMLHeadingElement}
   */
  $sittingLabel = document.getElementById('sitting-label');
  /**
   * @type {HTMLButtonElement}
   */
  $start = document.getElementById('start');
  /**
   * @type {HTMLButtonElement}
   */
  $pause = document.getElementById('pause');
  /**
   * @type {HTMLButtonElement}
   */
  $skip = document.getElementById('skip');
  /**
   * @type {HTMLButtonElement}
   */
  $stop = document.getElementById('stop');
  /**
   * @type {HTMLInputElement}
   */
  $standingDuration = document.getElementById('standing-duration');
  /**
   * @type {HTMLInputElement}
   */
  $sittingDuration = document.getElementById('sitting-duration');

  /**
   * @param {() => void} onStart
   * @param {() => void} onStop
   * @param {() => void} onPause
   * @param {() => void} onNext
   * @param {() => void} onChangeSittingValue
   * @param {() => void} onChangeStandingValue
   */
  constructor(
    onStart,
    onStop,
    onPause,
    onNext,
    onChangeSittingValue,
    onChangeStandingValue,
  ) {
    this.$start.addEventListener('click', onStart);
    this.$stop.addEventListener('click', onStop);
    this.$pause.addEventListener('click', onPause);
    this.$skip.addEventListener('click', onNext);

    this.$sittingDuration.addEventListener('change', onChangeSittingValue);
    this.$standingDuration.addEventListener('change', onChangeStandingValue);
  }

  /**
   * @param {'STOPPED'|'PAUSED'|'RUNNING'} currentStatus
   */
  renderButtonsDisabled(currentStatus) {
    switch (currentStatus) {
      case 'STOPPED':
        this.$start.removeAttribute('disabled');
        this.$stop.setAttribute('disabled', '');
        this.$pause.setAttribute('disabled', '');
        break;
      case 'PAUSED':
        this.$start.removeAttribute('disabled');
        this.$stop.removeAttribute('disabled');
        this.$pause.setAttribute('disabled', '');
        break;
      case 'RUNNING':
        this.$start.setAttribute('disabled', '');
        this.$stop.removeAttribute('disabled');
        this.$pause.removeAttribute('disabled');
        break;
    }
  }

  /**
   * @param {number} minutes
   */
  updateStandingValue(minutes) {
    this.$standingLabel.textContent = `Standing for ${minutes}m`;
  }

  /**
   * @param {number} minutes
   */
  updateSittingValue(minutes) {
    this.$sittingLabel.textContent = `Sitting for ${minutes}m`;
  }

  /**
   * @param {boolean} isSitting
   * @param {number} elapsedSeconds
   */
  renderHeader(isSitting, elapsedSeconds) {
    const text = isSitting
      ? `StandTimer: Sitting ${elapsedSeconds}`
      : `StandTimer: Standing ${elapsedSeconds}`;
    this.$heading.textContent = text;
    document.title = text;
  }

  getStandingDurationValue() {
    return parseInt(this.$standingDuration.value);
  }

  getSittingDurationValue() {
    return parseInt(this.$sittingDuration.value);
  }
}
